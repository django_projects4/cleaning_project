# Generated by Django 2.2.4 on 2022-06-06 08:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('clean', '0006_contactus'),
    ]

    operations = [
        migrations.CreateModel(
            name='EmailSubscription',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.CharField(max_length=80)),
            ],
        ),
    ]
