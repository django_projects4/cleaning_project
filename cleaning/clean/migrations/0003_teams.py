# Generated by Django 2.2.4 on 2022-06-06 06:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('clean', '0002_remove_projects_slug'),
    ]

    operations = [
        migrations.CreateModel(
            name='Teams',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=80)),
                ('image', models.ImageField(upload_to='')),
                ('designation', models.CharField(max_length=80)),
            ],
        ),
    ]
