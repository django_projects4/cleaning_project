# Generated by Django 2.2.4 on 2022-06-06 07:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('clean', '0003_teams'),
    ]

    operations = [
        migrations.CreateModel(
            name='Blogs',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=80)),
                ('image', models.ImageField(upload_to='')),
                ('description', models.TextField(max_length=500)),
                ('title1', models.CharField(max_length=80)),
                ('title2', models.CharField(max_length=80)),
                ('date', models.DateField()),
            ],
        ),
    ]
