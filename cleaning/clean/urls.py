from django.contrib import admin
from django.urls import path

from .import views
admin.site.site_header = 'Klean'                    # default: "Django Administration"
admin.site.index_title = 'Features area'                 # default: "Site administration"
#admin.site.site_title = 'HTML title from adminsitration' # default: "Django site admin"
urlpatterns = [
    path('', views.index, name='index'),


]
