from django.contrib import admin

# Register your models here.


from django.contrib import admin

from .models import Projects, Teams, Blogs,Contactus,EmailSubscription,Services

@admin.register(Projects)
class ProjectsAdmin(admin.ModelAdmin):
    list_display = ("id","name",)

@admin.register(Teams)
class TeamsAdmin(admin.ModelAdmin):
    list_display = ("id","name","designation")

@admin.register(Blogs)
class BlogsAdmin(admin.ModelAdmin):
    list_display = ("id","name","date", "title1","title2","description_heading")

@admin.register(Contactus)
class ContactusAdmin(admin.ModelAdmin):
    list_display = ("id","name","email", "subject")

@admin.register(EmailSubscription)
class EmailSubscriptionAdmin(admin.ModelAdmin):
    list_display = ("id","email")

@admin.register(Services)
class ServicesAdmin(admin.ModelAdmin):
    list_display = ("id","name", "price")