from django.db import models

# Create your models here.
from django.urls import reverse


class Projects(models.Model):
    name = models.CharField(max_length=80)
    image = models.ImageField()
    #slug = models.SlugField()


class Teams(models.Model):
    name = models.CharField(max_length=80)
    image = models.ImageField()
    designation=models.CharField(max_length=80)

class Blogs(models.Model):
    name = models.CharField(max_length=80)
    image = models.ImageField()
    description_heading = models.CharField(max_length=50)
    description=models.TextField(max_length=500)
    title1 = models.CharField(max_length=80)
    title2 = models.CharField(max_length=80)
    date=models.DateField()

class Contactus(models.Model):
    name = models.CharField(max_length=80)
    email = models.CharField(max_length=80)
    subject = models.CharField(max_length=80)
    message=models.TextField(max_length=500)

class EmailSubscription(models.Model):
    email = models.CharField(max_length=80)

class Services(models.Model):
    name = models.CharField(max_length=80)
    image = models.ImageField()
    price = models.FloatField(default=0.0)
