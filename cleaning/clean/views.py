from django.shortcuts import render

# Create your views here.
from .models import Projects,Teams,Blogs,Contactus,Services


def index(rerquest):
    projects = Projects.objects.all()
    teams = Teams.objects.all()
    blogs = Blogs.objects.all()
    print("projects", projects)
    services = Services.objects.all()
    context = {'projects': projects}
    return render(rerquest,'home.html',{"projects":projects, "teams":teams, "blogs":blogs, "services":services})

def contactus(request):
    if request.method == 'POST' and request.POST.get('name'):
        form = Contactus(request.POST)
        if form:
            data = Contactus.objects.create(
                name=request.POST.get('name'),
                email=request.POST.get('email'),
                subject=request.POST.get('subject'),
                message=request.POST.get('message'),

            )
            print("ddddddddddddddddddddd", data)
            return render(request, 'submit.html', {'message': "Thanks for contact"})
    return render(request,'contact.html')

def projects(request):
    projects = Projects.objects.all()

    context = {'projects': projects}
    return  render(request,'project.html',{"projects":projects})

def aboutus(request):
    teams = Teams.objects.all()
    return  render(request,'about.html',{"teams":teams})

def service(request):
    services = Services.objects.all()
    return render(request,'service.html', {"services":services})

def blogs(request):
    blogs = Blogs.objects.all()
    return render(request,'blog.html',{"blogs":blogs})
